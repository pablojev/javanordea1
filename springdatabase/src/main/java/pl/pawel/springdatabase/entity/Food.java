package pl.pawel.springdatabase.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Food {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private long id;
	private String name;
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "setOfFood")
	private Set<Animal> setOfAnimals;
	
	public Food() {}
	
	public Food(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Animal> getSetOfAnimals() {
		return setOfAnimals;
	}

	public void setSetOfAnimals(Set<Animal> setOfAnimals) {
		this.setOfAnimals = setOfAnimals;
	}
	
}
