package pl.pawel.springdatabase.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Animal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	@ManyToOne(cascade = CascadeType.MERGE)
	private Species species;
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "animal", cascade = CascadeType.ALL)
	private AnimalDetails animalDetails;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Food> setOfFood;
	
	public Animal() { }
	
	public Animal(String name) { 
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public AnimalDetails getAnimalDetails() {
		return animalDetails;
	}

	public void setAnimalDetails(AnimalDetails animalDetails) {
		this.animalDetails = animalDetails;
	}

	public Set<Food> getSetOfFood() {
		return setOfFood;
	}

	public void setSetOfFood(Set<Food> setOfFood) {
		this.setOfFood = setOfFood;
	}
	
	
}
