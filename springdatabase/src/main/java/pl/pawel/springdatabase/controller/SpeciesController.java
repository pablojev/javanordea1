package pl.pawel.springdatabase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.pawel.springdatabase.entity.Species;
import pl.pawel.springdatabase.repository.SpeciesRepository;

@Controller
@RequestMapping("/species")
public class SpeciesController {
	
	@Autowired
	SpeciesRepository speciesRepository;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addForm() {
		return "species/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addSpecies(@RequestParam("name") String name) {
		Species s = new Species(name);
		speciesRepository.save(s);
		return "index";
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") long id, ModelMap model) {
		speciesRepository.delete(id);
		model.addAttribute("msg", "Pomyślnie usunięto gatunek.");
		return "index";
	}
	

	
	
	
	
	
	
	
	
	
	
}
