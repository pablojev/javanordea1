package pl.pawel.springdatabase.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.pawel.springdatabase.entity.Animal;
import pl.pawel.springdatabase.entity.AnimalDetails;
import pl.pawel.springdatabase.entity.Food;
import pl.pawel.springdatabase.entity.Species;
import pl.pawel.springdatabase.repository.AnimalRepository;
import pl.pawel.springdatabase.repository.FoodRepository;
import pl.pawel.springdatabase.repository.SpeciesRepository;

@Controller
@RequestMapping("/animal")
public class AnimalController {
	
	@Autowired
	SpeciesRepository speciesRepository;
	
	@Autowired
	AnimalRepository animalRepository;
	@Autowired
	FoodRepository foodRepository;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addAnimal(ModelMap model) {
		List<Species> listOfSpecies = (List<Species>) speciesRepository.findAll();
		model.addAttribute("species", listOfSpecies);
		return "animal/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String handleAddAnimal(@RequestParam("name") String name,
								 @RequestParam("species") long speciesId,
								 @RequestParam("detailsName") String detailsName,
								 @RequestParam("kingdom") String kingdom,
								 @RequestParam("hasTail") boolean hasTail) {
		Species s = speciesRepository.findOne(speciesId);
		Animal a = new Animal(name);
		a.setSpecies(s);
		
		AnimalDetails animalDetails = new AnimalDetails(detailsName, kingdom, hasTail);
		animalDetails.setAnimal(a);
		
		a.setAnimalDetails(animalDetails);
		animalRepository.save(a);
		return "index";
	}
	
	@RequestMapping("/delete/{id}")
	public void delete(@PathVariable("id") long id,
					   HttpServletRequest request,
					   HttpServletResponse response) throws IOException {
		Animal a = animalRepository.findOne(id);
		Species s = a.getSpecies();
		List<Animal> listOfAnimals = s.getListOfAnimals();
		listOfAnimals.remove(a);
		speciesRepository.save(s);
		animalRepository.delete(a);
		response.sendRedirect(request.getContextPath() + "/");
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editAnimal(@PathVariable("id") long id, ModelMap model) {
		model.addAttribute("animal", animalRepository.findOne(id));
		model.addAttribute("species", speciesRepository.findAll());
		model.addAttribute("foods", foodRepository.findAll());
		return "animal/edit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String handleEditAnimal(@PathVariable("id") long id,
									@RequestParam("food") Long[] foodIds,
									ModelMap model) {
		Animal animal = animalRepository.findOne(id);
		// zwykla edycja pol w animalu
		animal.setSetOfFood(null);
		animalRepository.save(animal);
		
		Set<Food> setOfFood = new HashSet<>();
		for(long foodId : foodIds) {
			System.out.println("\t> " + foodId);
			setOfFood.add(foodRepository.findOne(foodId));
		}
		
		animal.setSetOfFood(setOfFood);
		animalRepository.save(animal);
		
		return "animal/edit";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
