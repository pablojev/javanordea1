package pl.pawel.springdatabase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.pawel.springdatabase.entity.Food;

@Repository
public interface FoodRepository extends CrudRepository<Food, Long>{

}
