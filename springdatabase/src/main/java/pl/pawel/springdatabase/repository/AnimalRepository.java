package pl.pawel.springdatabase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.pawel.springdatabase.entity.Animal;

@Repository
public interface AnimalRepository extends CrudRepository<Animal, Long> {

}
