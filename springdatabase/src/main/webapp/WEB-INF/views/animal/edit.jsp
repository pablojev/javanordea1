<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp" />

<form action='<c:url value="/animal/edit/${animal.id}" />' method="POST">
	<input type="text" name="name" value="${animal.name}">
	<select name="species">
		<c:forEach items="${species}" var="s">
		<option value="${s.id}">${s.name}</option>
		</c:forEach>
	</select>
	<hr>
	<input type="text" name="detailsName" value="${animal.animalDetails.name}">
	<input type="text" name="kingdom" value="${animal.animalDetails.kingdom}">
	<input type="checkbox" name="hasTail"> Posiadam ogon
	<hr>
	<c:forEach items="${foods}" var="oneFood">
		<c:forEach items="${animal.setOfFood}" var="animalFood">
		<c:if test="${oneFood.id eq animalFood.id}">
			<c:set value="checked='checked'" var="isChecked" />
		</c:if>
		</c:forEach>
		<p>
			<input type="checkbox" name="food" value="${oneFood.id}" ${isChecked} /> ${oneFood.name}
		</p>
		<c:set value="" var="isChecked" />
	</c:forEach>
	<button type="submit">Dodaj zwierzę</button>
</form>

<c:import url="../footer.jsp" />