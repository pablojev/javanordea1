<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp"/>
		<h2>Witam serdecznie.</h2>
		
		<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
		
		<c:if test="${species ne null}">
		<c:forEach items="${species}" var="s">
			<p>${s.name} <a href='<c:url value="/species/delete/${s.id}"/>'>Usuń</a></p>
			<ul>
			<c:forEach items="${s.listOfAnimals}" var="animal">
				<li>
					${animal.name}
					<div>
						<c:forEach items="${animal.setOfFood}" var="food">
							<span>${food.name} </span> 
						</c:forEach>
					</div>
					<a href='<c:url value="/animal/edit/${animal.id}" />'>Edytuj</a>
					<a href='<c:url value="/animal/delete/${animal.id}" />'>Usuń</a>
				</li>
			</c:forEach>
			</ul>
		</c:forEach>
		</c:if>
<c:import url="footer.jsp"/>