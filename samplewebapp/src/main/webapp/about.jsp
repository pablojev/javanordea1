<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp" />

<h3>Witam :)</h3>
<c:forEach items="${colors}" var="item">
    <p><c:out value="${item}" /></p>
</c:forEach>

<form action="LoginServlet" method="POST">
	<input type="hidden" name="action" value="login" />
	<input type="text" name="login" />
	<input type="password" name="pass" />
	<button type="submit">Wyślij</button>
</form>

<c:import url="footer.jsp" />