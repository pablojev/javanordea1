package pl.pawel.samplewebapp;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		String responseMessage = "Nie udało się zalogować";
		if (action.equals("logout")) {
			request.getSession().removeAttribute("loggedin");
			request.getSession().removeAttribute("login");
			responseMessage = "Logowanie poprawne";
		} else if (action.equals("login")) {
			String login = request.getParameter("login");
			String pass = request.getParameter("pass");
			if (login.equals("admin") && pass.equals("admin")) {
				request.getSession().setAttribute("loggedin", true);
				request.getSession().setAttribute("login", login);
			}
		}
		request.getServletContext().setAttribute("msg", responseMessage);
		response.sendRedirect("index.jsp");
	}

}
