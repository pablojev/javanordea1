package crud;

import org.hibernate.Session;

import util.HibernateUtil;

public class SimpleCRUD<T> implements CRUD<T> {
	
	private Class<T> clazz;
	
	public SimpleCRUD(Class<T> clazz) {
		this.clazz = clazz;
	}

	public T select(long id) {
		Session s = HibernateUtil.openSession();
		return s.get(clazz, id);
	}

}
