package pl.pawel.moviedb;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import crud.SimpleCRUD;
import pl.pawel.moviedb.entity.Genre;
import pl.pawel.moviedb.entity.Movie;
import util.HibernateUtil;

public class Main {
	public static void main(String[] args) {
//		Class<Movie> c = Movie.class;
//		try {
//			Movie movie = c.newInstance();
//			movie.setId(123L);
//			System.out.println(movie.getId());
//		} catch (InstantiationException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IllegalAccessException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		session.save(new Movie("Witam", 2000, 123, "opis", new Genre("Komedia")));
		tx.commit();
		
		
		SimpleCRUD<Movie> sc = new SimpleCRUD(Movie.class);
		Movie mm = sc.select(1L);
		System.out.println(mm.getTitle() + "  " + mm.getDuration());
		session.close();
		
		SimpleCRUD<Genre> sg = new SimpleCRUD(Genre.class);
		System.out.println(sg.select(2L).getName());
		
		System.exit(0);
		
		Transaction t = null;
		try {
			t = session.beginTransaction();
			// utworzylismy gatunek filmowy
			Genre g = new Genre("Comedy");
			// zapisujemy gatunek filmowy do sesji
			session.save(g);
			
			// tworzymy nowy film, z nowym gatunkiem
			Movie m = new Movie("Pierwszy film", 2000, 180, "Opis filmu", new Genre("Kryminał"));
			// tworzymy nowy film z gatunkiem, ktory juz w sesji juz istnieje
			Movie m2 = new Movie("Pierwszy film", 2000, 180, "Opis filmu", g);
			// zapisujemy oba filmy do sesji
			session.save(m);
			session.save(m2);
			// przesylamy zmiany 
			t.commit();
		} catch(Exception e) {
			if(t != null)
				t.rollback();
		}
		// deklaracja ziennej z nazwa gatunku (moze byc pobrana od uzytkownika)
		String findGenreByName = "Comedy";
		// tworzymy zapytanie HQL do sesji, z ktorej HIbernate wybierze nam rekordy
		List<Genre> gens = session.createQuery("from Genre WHERE name = '" + findGenreByName + "'").list();
		// sprawdzamy czy ilosc zwroconych rekordow jest wieksza od zera
		if(gens.size() > 0) {
			// jesli tak, oznacza to, ze gatunek podany w zmiennej juz istnieje w bazie danych
			// skoro tak - powinnismy go wykorzystac przy dodawaniu filmu
			System.out.println("Znanazłem: " + gens.get(0).getName());
		} else {
			// w przeciwnym wypadku, powinnismy dodac nowy gatunek filmu
			System.out.println("Nie znalazlem");
		}
		// zamkniecie sesji 
		session.close();
		
	}
}
