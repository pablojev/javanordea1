package pl.sdacademy.dbconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.sdacademy.dbconnector.resolver.CityResolver;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    		CityResolver cr = new CityResolver();
    		
    		Map<String, String> params = new HashMap<>();
    		params.put("id", "12");
    		params.put("name", "Elblag");
    		// insert
    		cr.insert(params);
    		
    		// getOne
    		City c = cr.get(12);
    		
    		System.out.println(c.getName());
    		
    		// getAll
    		List<City> list = cr.get();
    		
    		for(City cc : list) {
    			System.out.println(cc.getId() + ". " + cc.getName());
    		}
    		
    		// update
    		params.replace("name", "Nowy Elblag");
    		cr.update(12, params);
    		c = cr.get(12);
    		System.out.println("UPDATE: " + c.getName());
    		
    		// delete
    		cr.delete(12);
    }
    
   
}
