package pl.sdacademy.dbconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
	// 1. Deklaracja pól, wymaganych do połączenia z bazą danych
	// np. dbname, dbuser, dbhost, dbpass, dbport
	String dbhost = "localhost";
	String dbname = "GEO";
	String dbuser = "pablojev";
	String dbpass = "pablojev";
	int dbport = 5432;
	// 2. Utórzmy pole prywatny typu DBConnector, które będzie przetrzymywało
	// naszą instancję
	private static DBConnector _instance = null;
	private Connection _connection = null;
	// 3. Wykorzystajmy do tego wzorzec Singleton.

	private DBConnector() {

	}

	public static Connection getConnection() {
		if (_instance == null) {
			_instance = new DBConnector();
		}
		try {
			String url = "jdbc:postgresql://" + _instance.dbhost + ":" + _instance.dbport + "/" + _instance.dbname;
			_instance._connection = DriverManager.getConnection(url, _instance.dbuser, _instance.dbpass);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return _instance._connection;
	}
}
