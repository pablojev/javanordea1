<%@ page language="java" pageEncoding="UTF-8" 
		 contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:import url="header.jsp" />
<form action="./LoginServlet" method="POST">
	<input type="text" name="username" placeholder="Podaj login" />
	<input type="password" name="password" />
	<button type="submit">Zaloguj</button>
</form>
<c:import url="footer.jsp" />