<%@ page language="java" pageEncoding="UTF-8" 
		 contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<html>
<head>

</head>
<body>
<nav>
	<ul>
		<li><a href="./index.jsp">Strona główna</a></li>
		<c:if test="${sessionScope.loggedin}">
		<li><a href="./admin.jsp">Zarządzaj</a></li>
		<li><a href="./LoginServlet?action=logout">Wyloguj</a></li>
		</c:if>
		<c:if test="${sessionScope.loggedin ne true}">
		<li><a href="./login.jsp">Zaloguj</a></li>
		</c:if>
	</ul>
</nav>
<c:if test="${sessionScope.loggedin}">
	UWAGA JESTEM ZALOGOWANY!
</c:if>