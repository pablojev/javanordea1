package pl.pawel.moviedatabase.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import pl.pawel.moviedatabase.entity.Movie;
import pl.pawel.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class SingleMovieServlet
 */
public class SingleMovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SingleMovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long id = Long.valueOf(request.getParameter("id"));
		//response.getWriter().append("id: " + id);
		Session session = HibernateUtil.openSession();
		Movie movie = session.get(Movie.class, id);
		session.close();
		request.setAttribute("movie", movie);
		request.getRequestDispatcher("movie.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
