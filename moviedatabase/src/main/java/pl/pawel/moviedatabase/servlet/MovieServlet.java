package pl.pawel.moviedatabase.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.pawel.moviedatabase.entity.Movie;
import pl.pawel.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class MovieServlet
 */
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		//doGet(request, response);
		String title = request.getParameter("title");
		long year = Long.valueOf(request.getParameter("year"));
		String genre = request.getParameter("genre");
		// utworzmy obiekt reprezentujacy encje
		Movie m = new Movie();
		// ustawmy jego parametry setterami lub konstruktorem
		m.setTitle(title);
		m.setGenre(genre);
		m.setYear(year);
		// otworzmy sesje + transakcje
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		// zapiszmy obiekt
		session.save(m);
		// scommitujmy transakcje
		tx.commit();
		// zamknijmy sesje
		session.close();
		request.setAttribute("msg", "Film został dodany poprawnie.");
		request.getRequestDispatcher("add.jsp").forward(request, response);
		//response.getWriter().append(title).append(" " + year).append(" " + genre);
	}

}
