package pl.pawel.moviedatabase.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.pawel.moviedatabase.entity.Movie;
import pl.pawel.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class EditMovieServlet
 */
public class EditMovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditMovieServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long id = Long.valueOf(request.getParameter("id"));
		Session session = HibernateUtil.openSession();
		Movie m = session.get(Movie.class, id);
		session.close();
		request.setAttribute("movie", m);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		long id = Long.valueOf(request.getParameter("id"));
		String title = request.getParameter("title");
		long year = Long.valueOf(request.getParameter("year"));
		String genre = request.getParameter("genre");
		System.out.println(id + " " + title);
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		Movie m = session.load(Movie.class, id);
		m.setTitle(title);
		m.setYear(year);
		m.setGenre(genre);
		session.update(m);
		tx.commit();
		session.close();
		response.sendRedirect("./ViewServlet");
	}

}
