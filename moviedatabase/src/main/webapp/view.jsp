<%@ page language="java" contentType="text/html; charset=UTF-8" 
		 pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp" />
<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Tytuł filmu</th>
			<th>Rok produkcji</th>
			<th>Gatunek</th>
			<th>Akcje</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${movies}" var="movie">
		<tr>
			<td>${movie.id}</td>
			<td><a href="./SingleMovieServlet?id=${movie.id}">${movie.title}</a></td>
			<td>${movie.year}</td>
			<td>${movie.genre}</td>
			<td>
				<a href="./DeleteMovieServlet?id=${movie.id}">Usuń</a>
				<a href="./EditMovieServlet?id=${movie.id}">Edytuj</a>
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<c:import url="footer.jsp" />