<%@ page language="java" contentType="text/html; charset=UTF-8" 
		 pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp" />

<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>

<form action="./MovieServlet" method="POST">
	<div class="input-group">
		<span class="input-group-addon">Tytuł</span> 
		<input type="text" name="title" class="form-control" />
	</div>
	<div class="input-group">
		<span class="input-group-addon">Rok</span> 
		<input type="text" name="year" class="form-control" />
	</div>
	<div class="input-group">
		<span class="input-group-addon">Gatunek</span> 
		<input type="text" name="genre" class="form-control" />
	</div>
	<button class="btn btn-success">Dodaj</button>
</form>

<c:import url="footer.jsp" />