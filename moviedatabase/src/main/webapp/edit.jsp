<%@ page language="java" contentType="text/html; charset=UTF-8" 
		 pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp" />

<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>

<form action="./EditMovieServlet" method="POST">
	<input type="hidden" name="id" value="${movie.id}">
	<div class="input-group">
		<span class="input-group-addon">Tytuł</span> 
		<input type="text" name="title" class="form-control" value="${movie.title}" />
	</div>
	<div class="input-group">
		<span class="input-group-addon">Rok</span> 
		<input type="text" name="year" class="form-control" value="${movie.year}" />
	</div>
	<div class="input-group">
		<span class="input-group-addon">Gatunek</span> 
		<input type="text" name="genre" class="form-control" value="${movie.genre}" />
	</div>
	<button class="btn btn-success">Edytuj</button>
</form>

<c:import url="footer.jsp" />