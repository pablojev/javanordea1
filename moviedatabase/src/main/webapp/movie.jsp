<%@ page language="java" contentType="text/html; charset=UTF-8" 
		 pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp" />
<div class="row">
	<div class="col-md-4">
		
	</div>
	<div class="col-md-8">
		<h2>${movie.title}</h2>
		<p><strong>Rok produkcji:</strong> ${movie.year}</p>
	</div>
</div>
<c:import url="footer.jsp" />