package pl.pawel.springmvc.util;

import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pl.pawel.springmvc.model.LoginData;

public class JsonDataParser {
	public static String get(String key, String json) {
		JSONParser jp = new JSONParser();
		String ret = "";
		try {
			JSONObject jo = (JSONObject) jp.parse(json);
			ret = (String) jo.get(key);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	
	public static List convertToList(String json) {
		List<LoginData> listOfUsers = new LinkedList<>();
		JSONParser jp = new JSONParser();
		JSONObject jo;
		try {
			jo = (JSONObject) jp.parse(json);
			JSONArray results = (JSONArray) jo.get("results");
			for(int i = 0; i < results.size(); i++) {
				jo = (JSONObject) results.get(i);
				JSONObject userName = (JSONObject) jo.get("name");
				listOfUsers.add(new LoginData(userName.get("first").toString(), userName.get("last").toString()));
			}
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return listOfUsers;
	}
}
