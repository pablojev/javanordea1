package pl.pawel.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.pawel.springmvc.model.LoginData;
import pl.pawel.springmvc.repository.HelloRepository;
import pl.pawel.springmvc.util.HttpConnector;
import pl.pawel.springmvc.util.JsonDataParser;

@Controller
@RequestMapping("/")
public class HelloController {
	
	@RequestMapping("/witam")
	public String welcome(ModelMap model) {
		String json = HttpConnector.get("https://randomuser.me/api?results=2");
		String msg = JsonDataParser.get("status", json);
		List<LoginData> loginDataList = JsonDataParser.convertToList(json);
		model.addAttribute("message", msg);
		model.addAttribute("users", loginDataList);
		return "welcome";
	}
	
	@RequestMapping("/pokaz")
	public ModelAndView pokaz() {
		String msg = "To jest informacja z metody model-and-view";
		return new ModelAndView("welcome", "message", msg);
	}
	
	@RequestMapping("/drukuj/{id}")
	public ModelAndView print(@PathVariable("id") long id) {
		ModelAndView mav = new ModelAndView("drukuj");
		mav.addObject("message", "Twój id to: " + id);
		return mav;
	}
}
