package pl.sdacademy.students.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
	private static String dbhost = "localhost";
	private static String dbname = "GEO";
	private static String dbuser = "pablojev";
	private static String dbpass = "pablojev";
	private static int dbport = 5432;
	private static Connection _connection = null;

	private DBConnector() {
	}

	public static Connection getConnection() {
		// lazy initialization
		if(_connection == null) {
			try {
				String url = "jdbc:postgresql://" + dbhost + ":" + dbport + "/" + dbname;
				_connection = DriverManager.getConnection(url, dbuser, dbpass);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return _connection;
	}
}
