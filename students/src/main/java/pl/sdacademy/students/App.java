package pl.sdacademy.students;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import pl.sdacademy.students.model.Student;
import pl.sdacademy.students.resolver.student.StudentResolver;

public class App 
{
    public static void main( String[] args )
    {
        Scanner s = new Scanner(System.in);
        
        while(true) {
        		System.out.println("1. Pokaż rekordy");
        		System.out.println("2. Dodaj rekord");
        		System.out.println("3. Usuń rekord [id=]");
        		System.out.println("4. Nadpisz rekord [id=]");
        		System.out.println("5. Koniec");
        		
        		System.out.println("\nWybierz opcję:");
        		Integer opt = Integer.valueOf(s.nextLine());
        		StudentResolver se = new StudentResolver();
        		
        		switch(opt) {
        		case 1:
        			List<Student> listOfStudents = se.get();
        			for(Student stud : listOfStudents) {
        				System.out.println(stud);
        			}
        			break;
        		case 2:
        			System.out.println("Podaj imię i nazwisko: ");
        			String[] names = s.nextLine().split(" ");
        			Map<String, String> p = new HashMap<>();
        			p.put("name", names[0]);
        			p.put("lastName", names[1]);
        			se.insert(p);
        			break;
        		case 3:
        			System.out.println("Podaj id rekordu do usunięcia: ");
        			int deleteId = Integer.valueOf(s.nextLine());
        			se.delete(deleteId);
        			break;
        		case 4:
        			System.out.println("Podaj id rekordu do nadpisania: ");
        			int editId = Integer.valueOf(s.nextLine());
        			System.out.println("Podaj nowe imię i nazwisko: ");
        			String[] editNames = s.nextLine().split(" ");
        			Map<String, String> pa = new HashMap<>();
        			pa.put("name", editNames[0]);
        			pa.put("lastName", editNames[1]);
        			se.update(editId, pa);
        			break;
        		case 5:
        			System.exit(0);
        		}
        		
        }
    }
}
