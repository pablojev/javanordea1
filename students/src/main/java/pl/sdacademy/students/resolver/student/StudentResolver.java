package pl.sdacademy.students.resolver.student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.sdacademy.students.util.DBConnector;
import pl.sdacademy.students.helper.QueryHelper;
import pl.sdacademy.students.model.Student;
import pl.sdacademy.students.resolver.AbstractResolver;

public class StudentResolver extends AbstractResolver<Student> {
	
	private Connection connection = DBConnector.getConnection();
	private final String TABLE_NAME = "student";
	
	@Override
	public Student get(int id) {
		Student s = null;
		
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id = " + id;
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()) {
				s = new Student(rs.getInt("id"), rs.getString("name"), rs.getString("lastName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public List<Student> get() {
		List<Student> listOfstudents = new LinkedList<>();
		
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id > 0";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				listOfstudents.add(new Student(rs.getInt("id"), rs.getString("name"), rs.getString("lastName")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listOfstudents;
	}

	@Override
	public void delete(int id) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM " + TABLE_NAME + " WHERE id = " + id;
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insert(Map<String, String> params) {
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "INSERT INTO " + TABLE_NAME + " (name, lastname) VALUES ('" + params.get("name") + "', '" + params.get("lastName") + "')";
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(int id, Map<String, String> params) {
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			//String sql = "UPDATE " + TABLE_NAME + " SET name = '" + params.get("name") + "', lastName = '" + params.get("lastName") + "' WHERE id = " + id;
			params.put("id", "" + id);
			String sql = QueryHelper.update(TABLE_NAME, params);
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
