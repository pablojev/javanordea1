package pl.pawel.httpconnector;

import java.io.DataOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {

	public static void main(String[] args) throws IOException, ParseException {
		
		// http://database.dev4.it:54000/users
		
		//System.exit(0);
		
		HashMap<String, String> map = new HashMap<>();
		map.put("name", "Pawel");
		map.put("lastname", "testowy");
		
		System.out.println(map);
		
		// GET
		URL url = new URL("http://palo.ferajna.org/sda/wojciu/json.php?login=test");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		String userAgent = "Pawel/1.0";
		
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", userAgent);
		String result = "";
		int responseCode = connection.getResponseCode();
		System.out.println(responseCode);
		if(responseCode == 200) {
			InputStreamReader reader = new InputStreamReader(connection.getInputStream());
			Scanner s = new Scanner(reader);
			
			while(s.hasNextLine()) {
				result += s.nextLine();
			}
		}
		
		System.out.println(result);
		
		ObjectMapper om = new ObjectMapper();
		
		JsonResponse jr = om.readValue(result, JsonResponse.class);
		
		System.out.println(jr.getNumber());
		
//		JSONParser jp = new JSONParser();
//		JSONObject jo = (JSONObject) jp.parse(result);
//		JSONArray ja = (JSONArray) jo.get("results");
//		
//		for(int i = 0; i < ja.size(); i++) {
//			JSONObject jooo = (JSONObject) ja.get(i);
//			System.out.println(jooo.get("gender"));
//			JSONObject userDetails = (JSONObject) jooo.get("name");
//			System.out.println(userDetails.get("first") + " " + userDetails.get("last"));
//		}
		
		System.exit(0);
		// POST
		URL postURL = new URL("http://database.dev4.it:54000/users");
		HttpURLConnection postConnection = (HttpURLConnection) postURL.openConnection();
		
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("User-Agent", "P/1.0");
		
		String queryString = "name=gawel22&lastname=testowy33";
		// tylko dla POST
		postConnection.setDoOutput(true);
		DataOutputStream dos = new DataOutputStream(postConnection.getOutputStream());
		dos.writeBytes(queryString);
		dos.flush();
		dos.close();
		// koniec tylko dla POSTa
		
		Scanner sc = new Scanner(new InputStreamReader(postConnection.getInputStream()));
		
		String postRet = "";
		
		while(sc.hasNextLine()) {
			postRet += sc.nextLine();
		}
		
		System.out.println(postRet);
		
		//JSONParser jp = new JSONParser();
		//JSONObject jo = (JSONObject) jp.parse(postRet);
		
		//.args.System.out.println("Status: " + jo.get("status"));
	}

}
