package pl.pawel.hiber;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import pl.pawel.hiber.entity.Animal;
import pl.pawel.hiber.entity.AnimalType;

public class App {

	public static void main(String[] args) {
		System.out.println("Hello Word!");
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		
		// Konfiguracja
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		Transaction t = session.beginTransaction();
		// Tworzenie typu
		AnimalType animalType = new AnimalType();
		animalType.setType("Ptak");
		// Tworzenie zwierzat
		List<Animal> anims = new LinkedList<>();
		Animal a1 = new Animal("Mewa");
		a1.setAnimalType(animalType);
		Animal a2 = new Animal("Dodo");
		a2.setAnimalType(animalType);
		Animal a3 = new Animal("Wróbelek");
		a3.setAnimalType(animalType);
		anims.add(a1);
		anims.add(a2);
		anims.add(a3);
		// zapis
		session.save(a1);
		session.save(a2);
		session.save(a3);
		
		animalType.setAnimals(anims);
		session.save(animalType);
		t.commit();
		
		// drukujemy zwierzeta , ktore sa przypisane do typu
		AnimalType at = session.load(AnimalType.class, animalType.getId());
		List<Animal> listOfAnimals = at.getAnimals();
		for(Animal a : listOfAnimals) {
			System.out.println(a.getId() + ". " + a.getName());
		}
		
		// 
		t = session.beginTransaction();
		
		AnimalType birdType = session.get(AnimalType.class, 73L);
		Animal k = new Animal("Kormoran");
		k.setAnimalType(birdType);
		List<Animal> as = birdType.getAnimals();
		as.add(k);
		birdType.setAnimals(as);
		session.save(k);
		session.save(birdType);
		
		t.commit();
		
		session.close();
		
		/*
		AnimalType animalType = new AnimalType();
		animalType.setType("Gad");
		
		Animal animal = new Animal();
		animal.setName("Jaszczurka");
		Animal anim2 = new Animal();
		anim2.setName("Waz");
		
		List<Animal> anims = new LinkedList<>();
		anims.add(animal);
		anims.add(anim2);
		
		animalType.setAnimals(anims);
		session.save(animalType);
		
		t.commit();
		AnimalType mammal = session.get(AnimalType.class, animalType.getId());
		System.out.println(mammal.getType());
		System.out.println(mammal.getAnimals());	
		for(Animal a : mammal.getAnimals()) {
			System.out.println(a.getName());
		}
		mammal.getAnimals().add(new Animal("Ryjówka"));
		session.save(mammal);
		
		for(Animal a : mammal.getAnimals()) {
			System.out.println(a.getName());
		}
		session.close();
		
		*/
		
		/*
		Animal animal = new Animal("Słoń");
		
		// insert
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		
		session.save(animal);
		session.save(new Animal("Kot"));
		session.save(new Animal("Pies"));
		
		t.commit();
		
		
		// pobranie 
		
		Animal myAnimal = session.load(Animal.class, 2L);
		System.out.println(myAnimal);
		
		// update
		t = session.beginTransaction();
		myAnimal.setName("Super Słoń");
		session.save(myAnimal);
		t.commit();
		
		// pobieranie wielu
		List<Animal> animals = session.createQuery("from Animal").list();
		for(Animal anim : animals) {
			System.out.println(anim);
		}
		
		// delete
		t = session.beginTransaction();
		session.delete(myAnimal);
		t.commit();
		session.close();
		*/
	}

}
